

<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8" />
        <title>MicroCMS</title>
        <link rel="stylesheet" href="/css/style.css" />
    </head>
    <body>
        <header>
            <h1>MicroCMS</h1>
        </header>

        <?php foreach($articles as $article): ?>
            <article>
                <h2><?= $article->getTitle() ?></h2>
                <p><?= $article->getContent() ?></p>
            </article>
        <?php endforeach ?>

        <footer class="footer">
        <a href="https://github.com/bpesquet/OC-MicroCMS">MicroCMS</a> is a minimalistic CMS built as a showcase for modern PHP development.
    </footer>
    </body>
</html>
